# Back-end_Assessment

## Why did I used Selenium instead of beautiful soup?


### Because we can't enable javascript in beautifulSoup and Twitter needs javascript to create the page and get tweets.
### Because Scrapy can't enable javascript and because Twitter needs javascript to loads tweets, twitter shows a page that "You need to enable javascript" and that is the page that scrapy and beautiful soup can scrap from twitter which has no tweets in it.

## Twitter's inspect page and view page source doesn't match which is the reason that make our work hard to scrap tweets with scrapy and beautiful soup.
