import scrapy
from scrapy.item import Item, Feild
from scrapy.selector import HtmlXPathSelector

class Tweet(Item):
    tweet = Feild()

class TwitterCrawler(scrapy.Spider):
    
    name = "crawler"
    
    def get_url(self, user):
        twitter_url = ['https://www.Twitter.com/'+str(user)]
        return scrapy.Request(url=twitter_url, callback = self.parse)
            
    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        tweet = Tweet()
        tweet["tweet"] = hxs.select('//div[@lang=en]').extract()
        return tweet
    
if __name__ == '__main__':
    twitter_crawler = TwitterCrawler()
    response = twitter_crawler.get_url("elonmusk")
    with open("scrapy_data.txt", 'w') as file:
        file.write(str(twitter_crawler.parse(response)))      
  